#include<stdio.h>
#include<stdlib.h>
#include<omp.h>

//size is the size of an array
int size = 100;

//The path struct includes the number of steps for the path
//dir is the direction we head. It will be "Right", "Left", "Up", or "Down"
struct path{
  int counter;
  char dir[200][6];
};

struct path traverse(int array[size][size], int row, int col, struct path graph_path,int visited[size][size]){

  //We make the element in visited equal to 1
  visited[row][col] = 1;
  //If this isnt the element we are looking for we will recurse
  if(row != (size-1) || col != (size-1)){


    //We create two path to see what will happen if we go down and right
    struct path right;
    struct path down;

    //We adjust the counter for our new path to be very large
    //The reason for this is seen later
    right.counter = (size*size)+1;
    down.counter = (size*size)+1;

 
    //Here we recurse on the right graph   
    #pragma omp task
    {
      //We perform this only if the element to the right exists, is 1, and has not been visisted
      if((col != (size-1)) && (array[row][col+1]==1) && (visited[row][col+1]!=1)){
        //We copy the current path to right, increment right's counter and add to its dir[] array
        right = graph_path;
        right.counter = graph_path.counter + 1; 
        strcpy(right.dir[right.counter],"Right");
        //We recurse
        right =  traverse(array, row, col+1,right,visited); 
      }
    }
 
    //Here we recurse on the down graph
    #pragma omp task
    {
      //We perform this only if the element beneath exists, is 1, and has not been visited
      if((row != (size-1)) && (array[row+1][col]==1) && (visited[row+1][col]!=1)){
        //We copy the current path to down, increment down's counter and add to its dir[] array
        down = graph_path;
        down.counter = graph_path.counter + 1;
        strcpy(down.dir[down.counter],"Down");
        //We recurse
        down = traverse(array, row+1,col,down,visited);
      }
    }
    
    #pragma omp taskwait
    //Here we see whether going right or going down gives fewer steps
    //If the program did not go right (if the right element does not exist, is 0, or has been visited)
    //Then right.counter will be very large (size*size+1) and so the program will return down
    //The opposite if the program did not go down
    #pragma omp critical
    {
      if(down.counter <= right.counter){
        return down;
      }
      else{
        return right;
      }
  
    }
  }

  //If we are currently on the element we are looking for we return the path
  if(row == (size-1) && col == (size-1) ){
    return graph_path;
  }
    
}


int main(){
  int i,j;
  //array[][] stores the graph.
  //visited[][] stores whether an element has been visited
  //  if it is 0, then it has not been visited. Otherwise it is 1
  int array[size][size];
  int visited[size][size];

  //We initialize visited[][] to all zeros
  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      visited[i][j] = 0;
    }
  }

  //graph_path is the path that we will traverse on
  struct path graph_path;
  graph_path.counter=0;
  //shortest will be the shortest path
  struct path shortest;

  //We read in the graph
  FILE *f;
  f = fopen("hundred.dat","r");
  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      fscanf(f,"%1d",&array[i][j]);     
    }
    fscanf(f,"\n");
  }

  //We find the shortest graph using the recursive traverse() function
  //We do so in parallel
  #pragma omp parallel
  {
    #pragma omp single
    {
      shortest = traverse(array,0,0,graph_path,visited);
    }
  }

  //We have cur_row, cur_col in order to print the directions
  int cur_row = 0;
  int cur_col=0;

  //We print the number of steps
  printf("Steps: %d\n", shortest.counter);
  printf("At (0,0)\n");
  //We print each direction in the shortest path
  for(i=1;i<shortest.counter+1;i++){
    //If we go right, we add 1 to cur_col
    if(strcmp(shortest.dir[i],"Right")== 0){
      cur_col+=1;
    }
    //If we go down, we add 1 to cur_row
    else{
      cur_row+=1;
    }
    printf("Step:%3d %6s to (%d,%d)\n",i,shortest.dir[i],cur_row,cur_col);
  }

  return 0;
}
