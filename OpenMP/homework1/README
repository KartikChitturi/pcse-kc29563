When run without parallelization, the process took:
------------------------------------  
|  # of elements | Time in seconds |
|----------------------------------|
|    10          |   0.000939      |
|    100         |   0.0017352     |
|    1000        |   8.615817      |
|    10000       |                 |
------------------------------------

When a simple parallel directive is placed there is no speedup.
In fact with more threads, the program gets slower

The following is the amount of time with the for directive
with a 1000x1000 matrix
--------------------------------------
|  # of threads    | Time in seconds |
|------------------------------------|
|       8          |    1.471528     |
|       16         |    0.971803     |
|       32         |    0.648521     |
|       64         |    0.601896     |
--------------------------------------
We see there is a speedup with an increased number of threads
However the speedup is not as pronounced between 32 and 64 threads
This is because 32 cores are being used

The following is the amount of time it takes when both populating
the matrices and performing the multiplication is done in parallel
with a 1000x1000 matrix
--------------------------------------
|  # of threads    | Time in seconds |
|------------------------------------|
|       8          |    1.107477     |
|       16         |    0.912542     |
|       32         |    0.628648     |
|       64         |    0.600465     |
--------------------------------------

