#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <time.h>

int main(int argc, char *argv[]){
    double start = omp_get_wtime(); 
 
    if(argc != 3){
        printf("Must pass in number of elements.\n");
    }
    int N = atoi(argv[1]);
    int ntids = atoi(argv[2]);
    double a[N][N];
    double b[N][N];
    double c[N][N];
    
  
    srand(time(NULL));
 
    
    //Populate arrays
    int i,j,k;
    
    
   #pragma omp parallel num_threads(ntids)
    {
    #pragma omp for
    for(i=0;i<N;i++){
      for(j=0;j<N;j++){
        a[i][j] = rand()/ (double)RAND_MAX;
        b[i][j] = rand()/ (double)RAND_MAX;
        c[i][j] = 0.0;
      }
    }
    }
    
    //Multiply matrices
    #pragma omp parallel num_threads(ntids)
    {
    #pragma omp for
    for(i=0;i<N;i++){
      for(j=0;j<N;j++){
        for(k=0;k<N;k++){
          c[i][j] += a[i][k] * b[k][j];
        }
      }
    }
   }
    if(N <= 10){
    //Print matrix
     for(i=0;i<N;i++){
       for(j=0;j<N;j++){
          printf("%f ",c[i][j]);
        }
        printf("\n");
    }    
  } 

  
  
    double end = omp_get_wtime();
    printf("%f\n",(end-start));


    return 0;
}

