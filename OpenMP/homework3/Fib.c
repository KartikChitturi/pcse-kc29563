#include<stdio.h>
#include<stdlib.h>
#include <omp.h>

int fib(int n,int *done, int *FibSeq, omp_lock_t doneLock, omp_lock_t fibLock){ 
  int i, j;  
  //fib has already been calculated for n we just return FibSeq[n] withotu computation
  //Else we go through the following if statement 
  if(done[n] == 0){ 
    if (n<2){
      return n;
   }
    else{
      
      //We lock the done[] and FibSeq[] arrays so they can only be accessed
      //by the current task
      omp_set_lock(&doneLock);
      omp_set_lock(&fibLock);          
      #pragma omp task shared(i) firstprivate(n)
      {
      i=fib(n-1,done,FibSeq,doneLock,fibLock);
      printf("%d\n",n-1);
      }
      //We unlock the arrays to allow them to be accessed by the next task
      omp_unset_lock(&doneLock);
      omp_unset_lock(&fibLock);

      //We again lock the arrays so only the current task can access them
      omp_set_lock(&doneLock);
      omp_set_lock(&fibLock);
      #pragma omp task shared(j) firstprivate(n)
      {
      j=fib(n-2,done,FibSeq,doneLock,fibLock);
      printf("%d\n",n-2);
      }
      //We unlock the tasks
      omp_unset_lock(&doneLock);
      omp_unset_lock(&fibLock);
      #pragma omp taskwait
      FibSeq[n] = i+j;
      done[n] = 1;
    }
  }
  return FibSeq[n];
}

//We declare the done and FibSeq arrays globally
//done[] keeps track of which inegers the fibonnaci function has calculated
//If done[n]=0 then fib(n) has not been calculated. If done[n]=1 then it has been
//FibSeq[] is the array of values for fib(n)
int done[10];
int FibSeq[10];

int main(int argc, char **argv){
  int n = 10;
  int i;

  //We initalize both arrays to 0
  for(i=0;i<n;i++){
    done[i]=0;
    FibSeq[i]=0;
  }
 
  //We create a locks for the done[] and fibSeq[] arrays
  omp_lock_t doneLock;
  omp_lock_t fibLock; 

  //We initalize the arrays
  omp_init_lock(&doneLock);
  omp_init_lock(&fibLock);

  //We calculate fib(10) recursively here
  omp_set_dynamic(0);
  omp_set_num_threads(4);
  #pragma omp parallel shared(n,done)
  {
    #pragma omp single
    printf ("fib(%d) = %d\n", n, fib(n,done,FibSeq,doneLock,fibLock));
  }
  
  //We destroy the locks
  omp_destroy_lock(&doneLock);
  omp_destroy_lock(&fibLock);
}
