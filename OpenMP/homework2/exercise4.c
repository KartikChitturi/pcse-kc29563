#include<stdio.h>
#include<stdlib.h>
#include "omp.h"

void print_matrix(int size, double matrix[size][size]);

int main(int argc, char *argv[]){

  if(argc != 2){
    printf("Require size of matrix!\n");
    return -1;
  }

  int size = atoi(argv[1]);
  int i,j,k;

  double m_A[size][size];
  double m_B[size][size];
  double m_C[size][size];

  srand(time(NULL));



  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
        m_A[i][j] = rand()/ (double)RAND_MAX;
        m_B[i][j] = rand()/ (double)RAND_MAX;
        m_C[i][j] = 0.0;
    }
  }

if(size<=10){
  printf("Matrix A\n");
  print_matrix(size,m_A);
  printf("\n\nMatrix B\n");
  print_matrix(size,m_B);
}

double start_time, end_time;
start_time = omp_get_wtime();
double temp=0;
#pragma omp parallel 
{
//#pragma omp for
for(i=0;i<size;i++){
  for(j=0;j<size;j++){
    #pragma omp for reduction(+:temp) schedule(static)
    for(k=0;k<size;k++){
      temp += m_A[i][k] * m_B[k][j];
    }
  m_C[i][j] = temp;
  temp = 0;
  }
}
}
end_time = omp_get_wtime();

if(size<=10){
  printf("\n\nMatrix C=A*B \n");
  print_matrix(size,m_C);
}

printf("\nSize:%d  Time:%3.3f\n", size,end_time-start_time);


return 0;
}

void print_matrix(int size, double matrix[size][size]){
  int i,j; 
  for(i=0;i<size;i++){
    for(j=0;j<size;j++){
      printf("%3.3f ",matrix[i][j]);
    }
    printf("\n");
 }
  
}
