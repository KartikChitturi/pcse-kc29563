#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mpi.h"

void print_matrix(MPI_Comm commun, double random_fraction);
void transpose(MPI_Comm comm, double *random_fraction);

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);

  int rank,size;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  
  //Determine if the number of processes is correct. The number of processes must be of the form (2^n)*(2^n)=2^(2n) for some integer n
  double log_size = (double)log(size)/(double)log(2.0);
  //We see that log_size gives the the logarithm of the size base 2. Therefore log_size must be an even integer
  //The floor function tests to see if it is an integer 
  if((log_size / 2.0) != floor(log_size/2.0)){
    if(rank == 0){
      printf("Number of processes needs to be 2^(2n) for some n\n");
    }
    MPI_Finalize();
    return 0;
  } 

  //We create a random_fraction on each process
  double random_fraction;
  srand(time(NULL)+(rank*(double)RAND_MAX/size));
  random_fraction = (rand()/ (double)RAND_MAX);

  //We print the matrix as it is initially
  if(rank == 0){
    printf("Initial matrix:\n");
  }
  print_matrix(MPI_COMM_WORLD,random_fraction);
  if(rank == 0){
    printf("\n\n");
  }
  
  //We transpose the elements of the matrix
  transpose(MPI_COMM_WORLD,&random_fraction);
  //We print the matrix after being transposed
  if(rank == 0){
    printf("Transposed matrix:\n");
  } 
  print_matrix(MPI_COMM_WORLD,random_fraction); 
  if(rank == 0){
    printf("\n\n");
  }
  

  MPI_Finalize();
  
  return 0;
}

//transpose() recursively splits the matrix into 4 submatrices and switches the upper-right and lower-left submatrices
void transpose(MPI_Comm comm,double *random_fraction){
  int rank,size;
  int colour;
  MPI_Comm_rank(comm,&rank);
  MPI_Comm_size(comm,&size);

  //We find the size of each row and half of that size
  //This assumes a square matrix which we checked in the main function
  int row_size = (int) sqrt(size);
  int half_row_size = row_size / 2;
  int i,j;
  
  //This is our base case: if our submatrix is of size 1, we are done
  if(size == 1){
    return;
  } 

  //The following 4 for loops split each process by which of the four submatrices it belongs to. We designate this by colour
  
  //These are the processes that belong to the upper-left submatrix
  for(i=0;i<half_row_size;i++){
    for(j=0;j<half_row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 1;
      } 
    }
  }
  
  //These are the processes that belong to the upper-right submatrix
  for(i=0;i<half_row_size;i++){
    for(j=half_row_size;j<row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 2;
      }
    }
  }

  //These are the processes that belong to the lower-left submatrix  
  for(i=half_row_size;i<row_size;i++){
    for(j=0;j<half_row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 3;
      }
    }
  }

  //These are the processes that belong to the lower-right submatrix
  for(i=half_row_size;i<row_size;i++){
    for(j=half_row_size;j<row_size;j++){
      if(rank == (row_size*i+j)){
        colour = 4;
      }
    }
  }
 
  //We split the communicator into 4 sub-communicators by the colour (the four submatrices) 
  MPI_Comm sub_comm; 
  MPI_Comm_split(comm,colour,rank,&sub_comm);

  //We gather the elements of each subprocess to the local root process  
  double local_array[size*size/4];
  /*
  int local_root;
  if(((rank % row_size) < half_row_size) && (rank < (size/2))){local_root = 0;}
  else if(((rank % row_size) >= half_row_size) && (rank < (size/2))){local_root = half_row_size;}
  else if(((rank % row_size) < half_row_size) && (rank >= (size/2))){local_root = size/2;}
  else {local_root = (size/2) + half_row_size;}
*/
 
  double trans_scalar = (*random_fraction);
  MPI_Gather(&trans_scalar,1,MPI_DOUBLE,&local_array,1,MPI_DOUBLE,0,sub_comm);


  //We send the gathered array from the local root of the lower-left submatrix to the upper-right submatrix and vice versa
  double rec_array[size*size/4];  
  if(rank == half_row_size){
     MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,size/2,0,&rec_array,size*size/4,MPI_DOUBLE,size/2,77,comm,MPI_STATUS_IGNORE);
  }

  if(rank == size/2){
    MPI_Sendrecv(&local_array,size*size/4,MPI_DOUBLE,half_row_size,77,&rec_array,size*size/4,MPI_DOUBLE,half_row_size,0,comm,MPI_STATUS_IGNORE);
  }

  //We scatter the new array to each of the processes
  if(((rank % row_size) >= half_row_size) && (rank < (size/2))){ 
    MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,1,MPI_DOUBLE,0,sub_comm);
  }

  if(((rank % row_size) < half_row_size) && (rank >= (size/2))){  
    MPI_Scatter(&rec_array, 1,MPI_DOUBLE,&trans_scalar,size*size/4,MPI_DOUBLE,0,sub_comm);  
  }

  //Each subprocess replaces it's random_fraction with the newly recieved number
  *random_fraction = trans_scalar;

  //We recuerse on each of the 4 submatriex
  transpose(sub_comm,random_fraction);

}

//Takes in a comminicator and prints out a matrix of using the random_fraction from each process
void print_matrix(MPI_Comm commun, double random_fraction){ 
  int rank,size;
  MPI_Comm_rank(commun,&rank);
  MPI_Comm_size(commun,&size);
  double data[size];
  MPI_Gather(&random_fraction,1,MPI_DOUBLE,data,1,MPI_DOUBLE,0,commun);
  int i,j;

  if(rank == 0){
    for(i = 0; i<sqrt(size); i++){
      for(j = 0; j<sqrt(size);j++){
        printf("%f ",data[i*(int)sqrt(size)+j]);
      }
      printf("\n");
    }

  } 
 
 }

