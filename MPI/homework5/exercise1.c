#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);
  int rank,size;
  int proc_column_length = 8;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);

  if(rank==0){
    printf("Columns are of length %d\n\n",proc_column_length);
  }

  int proc_row = rank % proc_column_length;
  int proc_col = rank / proc_column_length;

  MPI_Comm column_comm;
  MPI_Comm row_comm;

  MPI_Comm_split(MPI_COMM_WORLD,proc_col,rank,&column_comm);
  MPI_Comm_split(MPI_COMM_WORLD,proc_row,rank,&row_comm);

  
  int array[2];
  if(proc_row == 0){
    array[1] = proc_col;
  } 
  if(proc_col == 0){
    array[0] = proc_row;
  }
  MPI_Bcast(&array[0],1,MPI_INT,0,row_comm);
  MPI_Bcast(&array[1],1,MPI_INT,0,column_comm);


  
  printf("Rank: %2d   Row:%2d   Column:%2d\n",rank,array[0],array[1]);
  


  MPI_Finalize();
  return 0;
}
