#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"
#include<math.h>

int main(int argc, char *argv[]){

  //We send and recieve 10 times. The message is an int that has message_size digits
  const int ping_pong_limit = 5;
  const int message_size = 5;
  int i;

  MPI_Init(&argc,&argv);
  int rank;
  int size;
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  int count = 0;
  int message;
  int other_rank = (rank + 1) % 2;
  int num;

  double start_time, end_time;  
  double total_time = 0;
  for(count = 0; count< ping_pong_limit; count++){

    //Create message
    num = 0;
    for(i=0; i< message_size;i++){
      num += (int) pow(10,i)*i;  
    }
    start_time = MPI_Wtime();
    if(rank == 0){
      message = num;
      MPI_Send(&message, 1, MPI_INT, other_rank, 0, MPI_COMM_WORLD);
      printf("%d sent %d to %d\n", rank, message, other_rank);
    }
    else if(rank == 1){
      MPI_Recv(&message, 1, MPI_INT, other_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      if(message != num){
        printf("Message was not recieved properly!\n");
        printf("Process 0 sent %d\n", num);
        printf("Process 1 recieved %d\n",message);
        count = ping_pong_limit;
      }
      else{
        printf("%d received %d from %d\n",rank, message, other_rank);
      }
    }
    end_time = MPI_Wtime();
    total_time += end_time - start_time;
  
  }

  
    if(rank == 1){
      printf("\n\nTotal time was %lf for %d trials.",total_time, ping_pong_limit);
      printf("Average time was %lf", total_time/(double) ping_pong_limit);
    }
    
 
  MPI_Finalize();
  return 0;
}

