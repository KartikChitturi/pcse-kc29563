#!/bin/bash
#SBATCH -J test # job name
#SBATCH -o test.o%j # output and error file name (%j expands to jobID)
#SBATCH -n 32 # total number of mpi tasks requested
#SBATCH -N 2 # total number of nodes
#SBATCH -p normal # queue (partition) -- normal, development, etc.
#SBATCH -t 01:30:00 # run time (hh:mm:ss) - 1.5 hours
#SBATCH -A PCSE-2016
ibrun ./exercise2
