#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"
#include<math.h>

int main(int argc, char *argv[]){

  int globalParam; 
  int rank;
  int size;
  int i;


  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);


  printf("Global Parameter?\n");
  scanf("%d", &globalParam);
  if(globalParam % 32 != 0){
    printf("Global Paramter must be a multiple of 32. Exiting...\n");
    return 0;
  }
  
  

  int local_val[globalParam]=0;
  int global_array[globalParam];
  
  for(i = 0 + rank*(globalParam/32);i<rank*(globalParam/32);i++){
    local_val += (int) pow(i,2); 
  }


  MPI_Finalize();
  return 0;
}
