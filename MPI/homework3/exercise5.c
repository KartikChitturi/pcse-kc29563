#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);
  int rank;
  int partner_rank = -1;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);

  int predecessor, successor;
  if(rank != 0){
    predecessor = rank -1;
  }
  else{
    predecessor = MPI_PROC_NULL;
  }

  if(rank != 31){
    successor = rank+1;
  }
  else{
    successor = MPI_PROC_NULL;
  }


  MPI_Sendrecv(&rank, 1, MPI_INT, successor, 0,&partner_rank, 1, MPI_INT, predecessor, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

  if(rank != 0){ 
    printf("Process %d recieved from process %d\n", rank, partner_rank);  
  }


  MPI_Finalize();
  return 0; 
}
