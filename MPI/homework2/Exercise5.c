#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char* argv[]){
  double randomfraction;
  int mytid;
  int ntids;
  double data[32];
  int rank,size;
  int i;
  
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD,&mytid);
  MPI_Comm_size(MPI_COMM_WORLD,&size);
  ntids = 32;

  srand((int)(mytid*(double)RAND_MAX/ntids));
  randomfraction = (rand()/ (double)RAND_MAX);
  printf("Rank: %d  Random: %f\n",mytid,randomfraction);
  
  MPI_Gather(&randomfraction, 1,MPI_DOUBLE,data,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  
  double max_frac=0;
  int max_index=0;
  
  if(mytid == 0){
    for(i=0;i<32;i++){
      if(data[i]>max_frac){
        max_frac=data[i];
        max_index = i;
      }
    }

    printf("\nThe maximum value was: %f  which was from processor #%d",max_frac,max_index); 
  }
  
  MPI_Finalize();
  
  return 0;
}

