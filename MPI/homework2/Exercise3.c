#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char* argv[]){
  double randomfraction;
  int mytid;
  int ntids;

  MPI_Init(&argc,&argv);
  ntids = 32;
  MPI_Comm_rank(MPI_COMM_WORLD, &mytid);
  srand((int)(mytid*(double)RAND_MAX/ntids));
  randomfraction = (rand()/ (double)RAND_MAX);
  printf("Rank: %d  Random: %f\n",mytid,randomfraction);  
  double global_max;
  MPI_Reduce(&randomfraction,&global_max,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);

  if(mytid == 0){
    printf("\nGlobal Max: %f\n\n",global_max);
  }

  MPI_Bcast(&global_max,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  printf("Rank: %d  Scaled Random: %f\n",mytid, randomfraction/global_max);

  MPI_Finalize();
  return 0;
}
