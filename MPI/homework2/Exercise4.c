#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char* argv[]){
  MPI_Init(&argc,&argv);
  int A[2];
  
  A[0] = 1;
  A[1] = 2;
  
  int ihavenoidea;

  MPI_Reduce(A,&ihavenoidea,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
  
  printf("%d\n",ihavenoidea);


  MPI_Finalize();
  return 0;
}
