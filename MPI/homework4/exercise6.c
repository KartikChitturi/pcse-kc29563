#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);
  
  int rank,size;
  double randomfraction,recfraction;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size);  
  if(size%2 == 1){
    printf("Need an even number of proccessors!\n");
    MPI_Finalize();
    return 0;
  }

  srand(time(NULL)+(rank*(double)RAND_MAX/size));
  randomfraction = (rand()/ (double)RAND_MAX);
  recfraction = randomfraction;
  
  int s,i;
  double start_time, end_time;
  start_time = MPI_Wtime();
  for(s=0;s<size/2;s++){
    //Even stage 
      //Send and recieve the random doubles
      if(rank % 2 == 0){
        MPI_Sendrecv(&randomfraction,1,MPI_DOUBLE,rank+1,0,&recfraction,1,MPI_DOUBLE,rank+1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      }
      else{
        MPI_Sendrecv(&randomfraction,1,MPI_DOUBLE,rank-1,0,&recfraction,1,MPI_DOUBLE,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      }
    //Compare (and possibly) swap the random doubles
      if(rank % 2 == 0){
        if(randomfraction > recfraction){
          randomfraction = recfraction;
        }
      }
      else{
        if(randomfraction < recfraction){
          randomfraction = recfraction;
        }
      }
    
    //Odd stage
      //Send and recieve the random doubles
      if((rank % 2 == 1) && (rank != size-1)){
        MPI_Sendrecv(&randomfraction,1,MPI_DOUBLE,rank+1,0,&recfraction,1,MPI_DOUBLE,rank+1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      }
      else if((rank % 2 == 0) && (rank != 0)){
        MPI_Sendrecv(&randomfraction,1,MPI_DOUBLE,rank-1,0,&recfraction,1,MPI_DOUBLE,rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
      }
      //Compare (and possibly swap) the random doubles
      if((rank % 2 == 1) && (rank != size-1)){
        if(randomfraction > recfraction){
          randomfraction = recfraction;
        }
      }
      if((rank % 2 == 0) && (rank != 0)){
        if(randomfraction < recfraction){
          randomfraction = recfraction;
        }
      }   
  }
  end_time = MPI_Wtime();
  
  //Gather the doubles from each processor 
  double data[size];
  MPI_Gather(&randomfraction,1,MPI_DOUBLE,data,1,MPI_DOUBLE,0,MPI_COMM_WORLD);
  if(rank == 0){
    for(i=0;i<size;i++){
      printf("%f  \n",data[i]);
    }
    printf("\n\n\n");  
  printf("Total time:%lf\n", end_time-start_time);
  }

  MPI_Finalize();
  return 0;
}
