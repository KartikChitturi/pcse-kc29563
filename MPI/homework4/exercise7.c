#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
  MPI_Init(&argc,&argv);
  
  int rank, size;

  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&size); 


  double randomfraction;
  srand(time(NULL)+(rank*(double)RAND_MAX/size));
  randomfraction = (rand()/ (double)RAND_MAX);
  
  double initial_data[size];
  MPI_Gather(&randomfraction,1,MPI_DOUBLE,initial_data,1,MPI_DOUBLE,0,MPI_COMM_WORLD);

  //We tell each processor its left and right partner
  int left_partner;
  if(rank != 0){
    left_partner = rank - 1;
  }
  else{
    left_partner = MPI_PROC_NULL;
  }
  int right_partner;
  if(rank != size - 1){ 
    right_partner = rank + 1;
  }
  else{
    right_partner = MPI_PROC_NULL;
  }
 
  //We send the randomfraction to the left and right processors 
  MPI_Request send_left, send_right;
  MPI_Isend(&randomfraction, 1, MPI_DOUBLE, left_partner,0, MPI_COMM_WORLD,&send_left);
  MPI_Isend(&randomfraction, 1, MPI_DOUBLE, right_partner,0, MPI_COMM_WORLD,&send_right);
 
  //We recieve the randomfractions and then wait til all processors have recieved 
  double rand_left = 0.0;
  double rand_right = 0.0;
  MPI_Request request[2];
  MPI_Irecv(&rand_left,1, MPI_DOUBLE, left_partner, 0, MPI_COMM_WORLD, &request[0]);
  MPI_Irecv(&rand_right,1, MPI_DOUBLE, right_partner, 0, MPI_COMM_WORLD, &request[1]); 
  MPI_Waitall(2,request,MPI_STATUSES_IGNORE);
  
  //We compute the averages
  double average;
  if(rank == 0 || rank == size-1){
    average = (randomfraction + rand_left + rand_right)/2.0; 
  }
  else{
    average = (randomfraction + rand_left + rand_right)/3.0;
  }

  double avg_data[size];
  MPI_Gather(&average,1,MPI_DOUBLE,avg_data,1,MPI_DOUBLE,0,MPI_COMM_WORLD);

  //We print out the initial array and then the averaged array
  int i;
  if(rank == 0){
    for(i=0;i<size;i++){
      printf("Rank: %d  Random Fraction: %f\n",i,initial_data[i]);
    }
    printf("\n\n\n");
    for(i=0;i<size;i++){
      printf("Rank: %d  Averaged Fraction: %f\n",i,avg_data[i]);
    }
  }
  
  MPI_Finalize();
  return 0;
}
