#include<stdio.h>
#include<stdlib.h>
#include "mpi.h"

int main(int argc, char *argv[]){
  int rank;
  int i;
  MPI_Init(&argc,&argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  for(i=(rank*1413)+2; i<((rank+1)*1413)+2;i++){
    if(2000000111%i == 0){
      printf("Factor: %d, %d\n",i,2000000111/i);
    }
  }
  MPI_Finalize();
  return 0;
}
